package com.javainuse.repo;

import com.javainuse.model.dompet.Dompet;
import org.springframework.data.repository.Repository;

public interface WalletRepo extends Repository<Dompet, Integer> {
}
