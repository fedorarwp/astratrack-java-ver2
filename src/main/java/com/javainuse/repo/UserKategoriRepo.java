package com.javainuse.repo;

import com.javainuse.model.userKategori.UserKategori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserKategoriRepo extends JpaRepository<UserKategori, Integer> {
    UserKategori findAllByUserIdAndKategoriId(int id_user, int id_kategori);
    List<UserKategori> findAllByUserId(int id_user);

    List<UserKategori> findAllByKategoriId(int id_kategori);

    //@Query("SELECT uk FROM UserKategori AS uk WHERE uk.user.id = (:id_user) AND uk.kategori.id = (:id_kategori) ORDER BY uk.date DESC")
    //    UserKategori findAllByUserIdAndKategoriId(@Param("id_user")int id_user, @Param("id_kategori")int id_kategori);
}
