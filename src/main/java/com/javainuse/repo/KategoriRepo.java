package com.javainuse.repo;

import com.javainuse.model.kategori.Kategori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KategoriRepo extends JpaRepository<Kategori, Integer> {
}
