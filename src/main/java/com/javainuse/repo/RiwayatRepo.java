package com.javainuse.repo;

import com.javainuse.model.riwayat.Riwayat;
import com.javainuse.model.userKategori.UserKategori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RiwayatRepo extends JpaRepository<Riwayat, Integer> {
    @Query("SELECT his FROM Riwayat AS his WHERE his.user.id = (:id) ORDER BY his.date DESC")
    List<Riwayat> findAllByUserId(@Param("id") int id_user);

    @Query("SELECT his FROM Riwayat AS his WHERE his.userKategori IN ?1")
    List<Riwayat> findAllByUserKategori(List<UserKategori> userKategoriList);
}
