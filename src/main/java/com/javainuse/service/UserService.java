package com.javainuse.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import com.javainuse.controller.RestApiController;
import com.javainuse.model.userKategori.UserKategori;
import com.javainuse.model.kategori.Kategori;
import com.javainuse.repo.KategoriRepo;
import com.javainuse.repo.RoleRepo;
import com.javainuse.model.role.Role;
import com.javainuse.model.dompet.Dompet;
import com.javainuse.repo.UserKategoriRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.javainuse.repo.UserRepo;
import com.javainuse.model.user.User;
import com.javainuse.DTO.UserDTO;

@Service
public class UserService implements UserDetailsService {

	public static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private RoleRepo roleRepo;

	@Autowired
	private UserKategoriRepo userKategoriRepo;

	@Autowired
	private KategoriRepo kategoriRepo;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String noHp) throws UsernameNotFoundException {
		logger.info("Logged in {}", noHp);
		User user = userRepo.findByNoHp(noHp);

		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + noHp);
		}
		return new org.springframework.security.core.userdetails.User(user.getNoHp(), user.getPassword(),
				getAuthority(user));
	}

	public User findCurrentUser() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String noHp = userDetails.getUsername();

		return userRepo.findByNoHp(noHp);
	}

	public User save(UserDTO user) {
		logger.info("Creating User {}", user.getNama());
		User newUser = new User();
		Dompet newDompet = new Dompet();
		Role newRole = new Role();
		List<Kategori> kategoriList = new ArrayList<>();


		newUser.setNoHp(user.getNoHp());
		newUser.setEmail(user.getEmail());
		newUser.setNama(user.getNama());
		newUser.setTanggalLahir(user.getTanggalLahir());
		newUser.setPhoto(user.getPhoto());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));

		newDompet.setSaldo(0);
		newUser.setDompet(newDompet);

		newRole = roleRepo.findById(1).get();
		newUser.setRole(newRole);

		kategoriList = kategoriRepo.findAll();
		List<UserKategori> userKategoriList = new ArrayList<>();
		for (Kategori kategori: kategoriList) {
			UserKategori newUserKategori = new UserKategori();
			newUserKategori.setKategori(kategori);
			newUserKategori.setUser(newUser);
			newUserKategori.setTotalPerkategori(0);
			userKategoriList.add(newUserKategori);
		}
		newUser.addUserKategori(userKategoriList);

		System.out.println(newUser);
		userRepo.save(newUser);

		return newUser;
	}

	private Set<SimpleGrantedAuthority> getAuthority(User user) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		String role = user.getRole().getRoleTitle();
		authorities.add(new SimpleGrantedAuthority("ROLE_"+role));
		return authorities;
	}

}