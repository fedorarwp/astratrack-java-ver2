package com.javainuse.controller;

import com.javainuse.model.responseHelper.ResponseErrorHelper;
import com.javainuse.model.responseHelper.ResponseHelper;
import com.javainuse.model.userKategori.UserKategori;
//import com.javainuse.repo.UserKategoriRepo;
import com.javainuse.model.user.User;
import com.javainuse.repo.UserKategoriRepo;
import com.javainuse.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/astratrack")
@CrossOrigin(origins = "*")
public class UserKategoriController {
    public static final Logger logger = LoggerFactory.getLogger(UserKategoriController.class);

    @Autowired
    private UserKategoriRepo userKategoriRepo;

    @Autowired
    private UserService userService;

    // -------------------Retrieve Single Total Per User Per Kategori------------------------------------------
    //by id
    @RolesAllowed({"ROLE_USER"})
    @RequestMapping(value = "/kategoriperuser/{id_kategori}", method = RequestMethod.GET)
    public ResponseEntity<?> getKategoriPerUser(@PathVariable("id_kategori") int id_kategori) {
        User user = userService.findCurrentUser();
        logger.info("Fetching Product with id user{}", user.getId());

        UserKategori userKategori = userKategoriRepo.findAllByUserIdAndKategoriId(user.getId(), id_kategori);

        return new ResponseEntity<>(userKategori, HttpStatus.OK);
    }
    // -------------------Get All Kategori Per User------------------------------------------
    @RolesAllowed({"ROLE_USER"})
    @RequestMapping(value = "/alluserkategori", method = RequestMethod.GET)
    public ResponseEntity<?> getKategoriPerUser() {
        User user = userService.findCurrentUser();
        logger.info("Fetching Product with id user{}", user.getId());

        try {
            List<UserKategori> userKategoriList = userKategoriRepo.findAllByUserId(user.getId());

            return new ResponseEntity<>(new ResponseHelper(true, "All user kategori is successfully retrieved!", userKategoriList), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

}
