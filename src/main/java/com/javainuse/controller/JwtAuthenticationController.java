package com.javainuse.controller;

import com.javainuse.repo.UserRepo;
import com.javainuse.model.responseHelper.ResponseErrorHelper;
import com.javainuse.model.responseHelper.ResponseHelper;
import com.javainuse.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javainuse.config.JwtTokenUtil;
import com.javainuse.model.auth.JwtRequest;
import com.javainuse.model.auth.JwtResponse;
import com.javainuse.DTO.UserDTO;
import com.javainuse.service.UserService;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepo userDao;

	@RequestMapping(value = "/astratrack/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		try {
			authenticate(authenticationRequest.getNoHp(), authenticationRequest.getPassword());

			final UserDetails userDetails = userService.loadUserByUsername(authenticationRequest.getNoHp());

			final String token = jwtTokenUtil.generateToken(userDetails);

			String role = userDetails.getAuthorities().stream()
					.map(GrantedAuthority::getAuthority)
					.collect(Collectors.joining());

			JwtResponse response = new JwtResponse(token, role);
			return new ResponseEntity<>(new ResponseHelper(true, "Log in successful!", response), HttpStatus.OK);
		}
		catch(Exception	e) {
			//return ResponseEntity.badRequest().body(e.getCause().getMessage());
			return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
		}


//		return ResponseEntity.ok(new JwtResponse(token, jwtrole));
	}

	@RequestMapping(value = "/astratrack/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
		try {
			final User users = userService.save(user);
			return new ResponseEntity<>(new ResponseHelper<User>(true, "Registered!", users), HttpStatus.CREATED);
		}
		catch(Exception e) {
			return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getCause().getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	private void authenticate(String noHP, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(noHP, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}