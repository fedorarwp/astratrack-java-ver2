package com.javainuse.controller;

import com.javainuse.repo.WalletRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/astratrack")
@CrossOrigin(origins = "*")
public class RestApiDompetController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiDompetController.class);

    @Autowired
    private WalletRepo walletDao;


    // -------------------Top Up Wallet-------------------------------------------

    //@RolesAllowed("ROLE_ADMIN", "ROLE_USER")
//    @RequestMapping(value = "/wallet/", method = RequestMethod.POST, produces="application/json")
//    public ResponseEntity<?> topUpWallet(@RequestBody DAOWallet wallet) throws SQLException, ClassNotFoundException {
//        logger.info("Top Up Wallet Sejumlah : {}", wallet.getSaldo());
//        DAOWallet newWallet = walletDao.findByNoHp(wallet.getNoHp());
//
//        if (newWallet == null) {
//            newWallet = new DAOWallet();
//            newWallet.setNoHp(wallet.getNoHp());
//            newWallet.setSaldo(wallet.getSaldo() + newWallet.getSaldo());
//            newWallet.setMetodeTopUp(wallet.getMetodeTopUp());
//            walletDao.save(wallet);
//        }
//        else {
//            newWallet.setNoHp(wallet.getNoHp());
//            newWallet.setSaldo(wallet.getSaldo() + newWallet.getSaldo());
//            newWallet.setMetodeTopUp(wallet.getMetodeTopUp());
//            walletDao.save(newWallet);
//        }
//        return new ResponseEntity<>("Top Up Wallet Berhasil! Saldo wallet anda saat ini: " + newWallet.getSaldo(), HttpStatus.OK);
//    }
}