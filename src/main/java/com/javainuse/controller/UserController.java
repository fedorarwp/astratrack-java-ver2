package com.javainuse.controller;

import com.javainuse.DTO.UserDTO;
import com.javainuse.model.responseHelper.ResponseErrorHelper;
import com.javainuse.model.responseHelper.ResponseHelper;
import com.javainuse.repo.UserRepo;
import com.javainuse.model.user.User;
import com.javainuse.service.FileService;
import com.javainuse.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/astratrack")
@CrossOrigin(origins = "*")
public class UserController {
    public static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private FileService fileService;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    // -------------------Retrieve All Users--------------------------------------------
    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/listusers", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> listAllUsers() {
//        List<User> users = userRepo.findAll();
//
//        return new ResponseEntity<>(users, HttpStatus.OK);

        try{
            List<User> users = userRepo.findAll();

            return new ResponseEntity<>(new ResponseHelper(true, "All users are shown", users), HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Retrieve Single User------------------------------------------
    //by id
    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/listusers/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("id") int id) {
        try {
            logger.info("Fetching user with id {}", id);
            User user = userRepo.findById(id).get();

            return new ResponseEntity<>(new ResponseHelper(true, "A user is successfully fetched", user), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Get Current User------------------------------------------
    //by id
    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/currentuser", method = RequestMethod.GET)
    public ResponseEntity<?> getCurrentUser() {
        try {
            User user = userService.findCurrentUser();
            return new ResponseEntity<>(new ResponseHelper(true, "The current user is successfully fetched", user), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------- Update User Profile ------------------------------------------------
    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/edituser", method = RequestMethod.POST)
    public ResponseEntity<?> updateUserProfile(@RequestBody UserDTO user) {
        try {
            User currentUser = userService.findCurrentUser();
            logger.info("Updating profile with id {}", currentUser.getId());

            currentUser.setNama(user.getNama());
            currentUser.setEmail(user.getEmail());
            currentUser.setTanggalLahir(user.getTanggalLahir());

            if (!user.getPassword().equals("")) {
                currentUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            } else {
                currentUser.setPassword(currentUser.getPassword());
            }

            userRepo.save(currentUser);
            return new ResponseEntity<>(new ResponseHelper(true, "Successfully Updated User Profile", currentUser), HttpStatus.CREATED);
        }
        catch (Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------upload photo--------------------------------------------
    //@RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/upload-file", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> uploadFile(@RequestParam("file")MultipartFile file) throws IllegalStateException, IOException {

        try {
            final String fileName = fileService.uploadFile(file);

            return new ResponseEntity<>(new ResponseHelper(true, "File upload success!", fileName), HttpStatus.CREATED);
        }
        catch(Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------load photo--------------------------------------------
    //@RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(value = "/files/{filename}:.+", produces= MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<Resource> loadFile(@PathVariable String filename){
        try {
            Resource file = fileService.load(filename);

            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
        }
        catch(Exception e) {
            return new ResponseEntity(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
