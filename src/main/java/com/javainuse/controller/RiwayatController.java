package com.javainuse.controller;

import com.javainuse.model.kategori.Kategori;
import com.javainuse.model.responseHelper.ResponseErrorHelper;
import com.javainuse.model.responseHelper.ResponseHelper;
import com.javainuse.model.riwayat.Riwayat;
import com.javainuse.model.user.User;
import com.javainuse.model.userKategori.UserKategori;
import com.javainuse.repo.KategoriRepo;
import com.javainuse.repo.RiwayatRepo;
import com.javainuse.repo.UserKategoriRepo;
import com.javainuse.repo.UserRepo;
import com.javainuse.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/astratrack")
@CrossOrigin(origins = "*")
public class RiwayatController {
    public static final Logger logger = LoggerFactory.getLogger(RiwayatController.class);

    @Autowired
    private RiwayatRepo riwayatRepo;

    @Autowired
    private UserService userService;

    @Autowired
    private KategoriRepo kategoriRepo;

    @Autowired
    private UserKategoriRepo userKategoriRepo;

    @Autowired
    private UserRepo userRepo;

    // -------------------Create a Riwayat-------------------------------------------
    @RolesAllowed({"ROLE_USER"})
    @RequestMapping(value = "/riwayat/{id_kategori}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createRiwayatNew(@PathVariable("id_kategori") int id_kategori , @RequestBody Riwayat riwayat){
        try {
            User user = userService.findCurrentUser();
            Riwayat newRiwayat = new Riwayat();
            UserKategori userKategori = new UserKategori();
            logger.info("Creating Riwayat : {}", riwayat);

            newRiwayat.setTotal(riwayat.getTotal());
            newRiwayat.setTipe(riwayat.getTipe());
            newRiwayat.setStatus(riwayat.getStatus());
            newRiwayat.setNamaRiwayat(riwayat.getNamaRiwayat());
            newRiwayat.setIconRiwayat(riwayat.getIconRiwayat());


                double updateSaldo = 0;

                if (Objects.equals(riwayat.getTipe(), "pengeluaran")) {
                    if (user.getDompet().getSaldo() > newRiwayat.getTotal()) {
//                        Kategori kategori = kategoriRepo.findById(id_kategori).get();
                        userKategori = userKategoriRepo.findAllByUserIdAndKategoriId(user.getId(), id_kategori);
                        updateSaldo = user.getDompet().getSaldo() - riwayat.getTotal();
                    }
                    else {
                        return new ResponseEntity<>(new ResponseErrorHelper("Saldo tidak mencukupi"), HttpStatus.BAD_REQUEST);
                    }
                }
                else if (Objects.equals(riwayat.getTipe(), "pemasukan")) {
                    userKategori = userKategoriRepo.findAllByUserIdAndKategoriId(user.getId(), 1);
                    updateSaldo = user.getDompet().getSaldo() + riwayat.getTotal();
                }
                user.getDompet().setSaldo(updateSaldo);

                double updateTotalPerKategori = userKategori.getTotalPerkategori() + riwayat.getTotal();
                userKategori.setTotalPerkategori(updateTotalPerKategori);

                newRiwayat.setUserKategori(userKategori);
                newRiwayat.setUser(user);
                riwayatRepo.save(newRiwayat);

                return new ResponseEntity<>(new ResponseHelper(true, "New riwayat is successfully created!", newRiwayat), HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Retrieve All Riwayat Per User------------------------------------------
    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/riwayat", method = RequestMethod.GET)
    public ResponseEntity<?> getAllRiwayat() {
        try {
            logger.info("Fetching All Riwayat");
            List<Riwayat> riwayatList = riwayatRepo.findAll();

            return new ResponseEntity<>(new ResponseHelper(true, "All riwayat is successfully retrieved!", riwayatList), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Retrieve All Riwayat Per User------------------------------------------
    @RolesAllowed("ROLE_USER")
    @RequestMapping(value = "/riwayatuser", method = RequestMethod.GET)
    public ResponseEntity<?> getRiwayat() {
        try {
            User user = userService.findCurrentUser();
            logger.info("Fetching Riwayat for User with id {}", user.getId());
            List<Riwayat> riwayatList = riwayatRepo.findAllByUserId(user.getId());

            return new ResponseEntity<>(new ResponseHelper(true, "All riwayat is successfully retrieved!", riwayatList), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Edit Kategori for Riwayat------------------------------------------
    @RolesAllowed("ROLE_USER")
    @RequestMapping(value = "/editkategori/{id}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> updateKategoriForRiwayat(@PathVariable("id") int riwayat_id, @RequestBody Kategori kategori) {
        try {
            User user = userService.findCurrentUser();
            logger.info("Updating kategori riwayat with user id {}", user.getId());

            Riwayat riwayat = riwayatRepo.findById(riwayat_id).get();
            logger.info("RIWAYAT {}", riwayat);
            logger.info("ID KATEGORI {}", kategori.getId());
            UserKategori userKategori = riwayat.getUserKategori();
            UserKategori newUserKategori = userKategoriRepo.findAllByUserIdAndKategoriId(user.getId(), kategori.getId());
            logger.info("cek {}", newUserKategori.getId());

            userKategori.setTotalPerkategori(userKategori.getTotalPerkategori() - riwayat.getTotal());
            userKategoriRepo.save(userKategori);

            newUserKategori.setTotalPerkategori(newUserKategori.getTotalPerkategori() + riwayat.getTotal());
            userKategoriRepo.save(newUserKategori);

            riwayat.setUserKategori(newUserKategori);
            riwayatRepo.save(riwayat);

            return new ResponseEntity<>(new ResponseHelper(true, "Kategori is successfully edited!", riwayat), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Retrieve All Riwayat Per User Per Kategori------------------------------------------
    @RolesAllowed("ROLE_USER")
    @RequestMapping(value = "/riwayatuserkategori/{id_kategori}", method = RequestMethod.GET)
    public ResponseEntity<?> getRiwayatPerkategori(@PathVariable("id_kategori") int id_kategori) {
        try {
            User user = userService.findCurrentUser();
            logger.info("Fetching Riwayat for User with id kategori {}", user.getId());
            UserKategori userKategori = userKategoriRepo.findAllByUserIdAndKategoriId(user.getId(), id_kategori);

            List<Riwayat> riwayatList = userKategori.getRiwayatList();

            return new ResponseEntity<>(new ResponseHelper(true, "All riwayat is successfully retrieved!", riwayatList), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Retrieve All Riwayat By User Id------------------------------------------
    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/riwayatuserbyid/{id_user}", method = RequestMethod.GET)
    public ResponseEntity<?> getRiwayatById(@PathVariable("id_user") int id_user) {
        try {
            User user = userRepo.findById(id_user).get();
            logger.info("Fetching Riwayat for User with id {}", user.getId());
            List<Riwayat> riwayatList = riwayatRepo.findAllByUserId(user.getId());

            return new ResponseEntity<>(new ResponseHelper(true, "All riwayat is successfully retrieved!", riwayatList), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    // -------------------Retrieve All Riwayat By Kategori Id------------------------------------------
    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/riwayatuserbykategori/{id_kategori}", method = RequestMethod.GET)
    public ResponseEntity<?> getRiwayatByKategori(@PathVariable("id_kategori") int id_kategori) {
        try {
            Kategori kategori = kategoriRepo.findById(id_kategori).get();
            logger.info("Fetching Riwayat for Kategori with id {}", kategori.getId());
            List<UserKategori> userKategoriList = userKategoriRepo.findAllByKategoriId(kategori.getId());
            List<Riwayat> riwayatList = riwayatRepo.findAllByUserKategori(userKategoriList);

            return new ResponseEntity<>(new ResponseHelper(true, "All riwayat is successfully retrieved!", riwayatList), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

}
