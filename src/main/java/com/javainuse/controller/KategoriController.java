package com.javainuse.controller;

import com.javainuse.model.responseHelper.ResponseErrorHelper;
import com.javainuse.model.responseHelper.ResponseHelper;
import com.javainuse.model.riwayat.Riwayat;
import com.javainuse.model.userKategori.UserKategori;
import com.javainuse.model.kategori.Kategori;
import com.javainuse.model.user.User;
import com.javainuse.repo.KategoriRepo;
import com.javainuse.repo.RiwayatRepo;
import com.javainuse.repo.UserKategoriRepo;
import com.javainuse.repo.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/astratrack")
@CrossOrigin(origins = "*")
public class KategoriController {
    public static final Logger logger = LoggerFactory.getLogger(KategoriController.class);

    @Autowired
    private KategoriRepo kategoriRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserKategoriRepo userKategoriRepo;

    @Autowired
    private RiwayatRepo riwayatRepo;

    // -------------------Retrieve All Categories--------------------------------------------
    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/kategori", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> listAllKategori() {
        try {
            List<Kategori> kategoris = kategoriRepo.findAll();

            return new ResponseEntity<>(new ResponseHelper(true, "All categories are shown", kategoris), HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    // -------------------Create a Category-------------------------------------------
    @RolesAllowed({"ROLE_ADMIN"})
    @RequestMapping(value = "/kategori", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createNewKategori(@RequestBody Kategori kategori) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", kategori);

        try{
            List<String> judulKategoriList = new ArrayList<>();
            List<Kategori> kategoriList = kategoriRepo.findAll();
            for (Kategori eachKategori: kategoriList) {
                judulKategoriList.add(eachKategori.getJudulKategori().toLowerCase());
            }
            if (!judulKategoriList.contains(kategori.getJudulKategori().toLowerCase())) {
                List<User> userList = new ArrayList<>();
                userList = userRepo.findAll();
                for (User user: userList) {
                    UserKategori newUserKategori = new UserKategori();
                    newUserKategori.setKategori(kategori);
                    newUserKategori.setUser(user);
                    newUserKategori.setTotalPerkategori(0);
                    userKategoriRepo.save(newUserKategori);
                }
                kategoriRepo.save(kategori);
                return new ResponseEntity<>(new ResponseHelper(true, "Successfully Created a Category", kategori), HttpStatus.CREATED);
            }
            return new ResponseEntity<>(new ResponseErrorHelper("Couldn't add category because of duplicate value"), HttpStatus.CREATED);
        }
        catch(Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);

        }


    }

    // ------------------- Edit a Category ------------------------------------------------
    @RolesAllowed({"ROLE_ADMIN"})
    @RequestMapping(value = "/kategori/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateKategori(@PathVariable("id") int id, @RequestBody Kategori kategori) {
        logger.info("Updating Kategori with id {}", id);

        try {
            boolean flag = true;
            List<Kategori> kategoriList = kategoriRepo.findAll();
            Kategori currentKategori = kategoriRepo.findById(id).get();
            String currentJudulKategori = currentKategori.getJudulKategori();
            String newJudulKategori = kategori.getJudulKategori();

            if (!currentJudulKategori.equalsIgnoreCase(newJudulKategori)) {
                for (Kategori eachKategori: kategoriList) {
                    System.out.println("Nama current kategori " + currentJudulKategori);
                    System.out.println("Nama kategori baru " + newJudulKategori);
                    System.out.println("Pembandingnya " + eachKategori.getJudulKategori());
                    kategoriList.remove(newJudulKategori);
                    if (newJudulKategori.equalsIgnoreCase(eachKategori.getJudulKategori())) {
                        flag = true;
                        break;
                    } else {
                        flag = false;
                    }
                }

                if (flag) {
                    //kalo judul yang baru sama kayak yang yang ada di list kategori
                    return new ResponseEntity<>(new ResponseHelper (false, "Couldn't edit category because of duplicate value", kategoriList),  HttpStatus.BAD_REQUEST);
                } else {
                    //kalo judul yang baru belom dipake kategori lain
                    currentKategori.setJudulKategori(newJudulKategori);
                    currentKategori.setId(id);
                    currentKategori.setIconKategori(kategori.getIconKategori());

                    kategoriRepo.save(currentKategori);
                    return new ResponseEntity<>(new ResponseHelper(true, "Successfully Updated a Category", currentKategori), HttpStatus.CREATED);
                }
            }

            //kalo nama yang baru sama yang sekarang
            currentKategori.setJudulKategori(newJudulKategori);
            currentKategori.setId(id);
            currentKategori.setIconKategori(kategori.getIconKategori());

            kategoriRepo.save(currentKategori);
            return new ResponseEntity<>(new ResponseHelper(true, "Successfully Updated a Category", currentKategori), HttpStatus.CREATED);

        }
        catch (Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

//    // -------------------Get the Category for Each Riwayat--------------------------------------------
    @RolesAllowed({"ROLE_USER"})
    @RequestMapping(value = "/kategoriforriwayat/{id_riwayat}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> kategoriForRiwayat(@PathVariable("id_riwayat") int id_riwayat) {
        try {
            Riwayat riwayat = riwayatRepo.findById(id_riwayat).get();
            UserKategori userKategori = riwayat.getUserKategori();
            Kategori kategori = userKategori.getKategori();
            return new ResponseEntity<>(new ResponseHelper(true, "The category for sepcific riwayat is retrieved", kategori), HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

}
