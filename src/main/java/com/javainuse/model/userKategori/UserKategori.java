package com.javainuse.model.userKategori;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.kategori.Kategori;
import com.javainuse.model.riwayat.Riwayat;
import com.javainuse.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_kategori")
public class UserKategori {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private double totalPerkategori;

    public UserKategori(){};

    public UserKategori(int id, double total_perkategori) {
        this.id = id;
        this.totalPerkategori = total_perkategori;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalPerkategori() {
        return totalPerkategori;
    }

    public void setTotalPerkategori(double totalPerkategori) {
        this.totalPerkategori = totalPerkategori;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_user")
    @JsonIgnoreProperties("userKategoriList")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_kategori")
    @JsonIgnoreProperties("userKategoriList")
    private Kategori kategori;

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }


    @OneToMany(mappedBy = "userKategori", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("userKategori")
    List<Riwayat> riwayatList = new ArrayList<>();

    public List<Riwayat> getRiwayatList() {
        return riwayatList;
    }

    public void setRiwayatList(List<Riwayat> riwayatList) {
        this.riwayatList = riwayatList;
    }

    public void addRiwayat(List<Riwayat> riwayatList) {
        this.riwayatList = riwayatList;
        riwayatList.forEach(riwayat -> riwayat.setUserKategori(this));
    }
}
