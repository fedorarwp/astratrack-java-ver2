package com.javainuse.model.dompet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.user.User;

import javax.persistence.*;

@Entity
@Table(name = "dompet")
public class Dompet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column
    private double saldo;

    public Dompet(){};

    public Dompet(int id, double saldo) {
        this.id = id;
        this.saldo = saldo;
    }

    public int getIdWallet() {
        return id;
    }

    public void setIdWallet(int id) {
        this.id = id;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @OneToOne(mappedBy = "dompet", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("dompet")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
