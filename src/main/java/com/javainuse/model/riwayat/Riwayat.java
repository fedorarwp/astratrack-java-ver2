package com.javainuse.model.riwayat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.user.User;
import com.javainuse.model.userKategori.UserKategori;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@Table(name = "riwayat")
public class Riwayat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private double total;
    @Column
    private Date date = Date.valueOf(java.time.LocalDate.now());
    @Column
    private Time time = Time.valueOf(java.time.LocalTime.now());
    @Column
    private String namaRiwayat;
    @Column
    private String tipe;
    @Column
    private String status;
    @Column
    private String iconRiwayat;

    public Riwayat () {};

    public Riwayat(int id, double total, Date date, Time time, String namaRiwayat, String tipe, String status, String iconRiwayat) {
        this.id = id;
        this.total = total;
        this.date = date;
        this.time = time;
        this.namaRiwayat = namaRiwayat;
        this.tipe = tipe;
        this.status = status;
        this.iconRiwayat = iconRiwayat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getNamaRiwayat() {
        return namaRiwayat;
    }

    public void setNamaRiwayat(String namaRiwayat) {
        this.namaRiwayat = namaRiwayat;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIconRiwayat() {
        return iconRiwayat;
    }

    public void setIconRiwayat(String iconRiwayat) {
        this.iconRiwayat = iconRiwayat;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_user")
    @JsonIgnoreProperties("riwayatList")
    private User user;

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_user_kategori")
    @JsonIgnoreProperties("riwayatList")
    private UserKategori userKategori;

    @JsonIgnore
    public UserKategori getUserKategori() {
        return userKategori;
    }

    public void setUserKategori(UserKategori userKategori) {
        this.userKategori = userKategori;
    }
}
