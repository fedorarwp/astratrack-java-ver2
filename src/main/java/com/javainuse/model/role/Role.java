package com.javainuse.model.role;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String roleTitle;

    public Role(){};

    public Role(int id, String roleTitle) {
        this.id = id;
        this.roleTitle = roleTitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("role")
    List<User> userList = new ArrayList<>();

    public void addUser(List<User> userList) {
        this.userList = userList;
        userList.forEach(user -> user.setRole(this));
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
