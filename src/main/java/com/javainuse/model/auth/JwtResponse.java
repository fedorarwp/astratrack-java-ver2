package com.javainuse.model.auth;

import java.io.Serializable;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String token;
	private String role;

	public JwtResponse(String token, String role) {
		this.token = token;
		this.role = role;
	}

	public String getToken() {
		return this.token;
	}

	public String getRole() {
		return role;
	}
}