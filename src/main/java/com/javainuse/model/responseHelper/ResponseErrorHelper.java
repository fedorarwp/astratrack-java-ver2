package com.javainuse.model.responseHelper;

public class ResponseErrorHelper {
    private String message;
    private int code = 400;

    public ResponseErrorHelper(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ResponseErrorHelper(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
