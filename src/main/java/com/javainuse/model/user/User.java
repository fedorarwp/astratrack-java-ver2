package com.javainuse.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.userKategori.UserKategori;
import com.javainuse.model.riwayat.Riwayat;
import com.javainuse.model.role.Role;
import com.javainuse.model.dompet.Dompet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "user")
public class User implements UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String nama;
	@Column(unique = true)
	private String noHp;
	@Column(unique = true)
	private String email;
	@Column
	private LocalDate tanggalLahir;
	@Column
	private String photo;
	@Column
	@JsonIgnore
	private String password; // dalam format pin 6 digit

	public User(){};

	public User(int id, String nama, String noHp, String email, LocalDate tanggalLahir, String password, String photo) {
		this.id = id;
		this.nama = nama;
		this.noHp = noHp;
		this.email = email;
		this.tanggalLahir = tanggalLahir;
		this.password = password;
		this.photo = photo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(LocalDate tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="id_dompet")
	@JsonIgnoreProperties("user")
	private Dompet dompet;

	public Dompet getDompet() {
		return dompet;
	}

	public void setDompet(Dompet dompet) {
		this.dompet = dompet;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_role")
	@JsonIgnoreProperties("userList")
	private Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("user")
	List<UserKategori> userKategoriList = new ArrayList<>();

	public List<UserKategori> getUserKategoriList() {
		return userKategoriList;
	}

	public void setUserKategoriList(List<UserKategori> userKategoriList) {
		this.userKategoriList = userKategoriList;
	}

	public void addUserKategori(List<UserKategori> userKategoriList) {
		this.userKategoriList = userKategoriList;
		userKategoriList.forEach(userKategori -> userKategori.setUser(this));
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties("user")
	List<Riwayat> riwayatList = new ArrayList<>();

	public List<Riwayat> getRiwayatList() {
		return riwayatList;
	}

	public void setRiwayatList(List<Riwayat> riwayatList) {
		this.riwayatList = riwayatList;
	}

	public void addRiwayat(List<Riwayat> riwayatList) {
		this.riwayatList = riwayatList;
		riwayatList.forEach(riwayat -> riwayat.setUser(this));
	}

	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();

		Role role = new Role();
		authorities.add(new SimpleGrantedAuthority(role.getRoleTitle()));

		return authorities;
	}

	@JsonIgnore
	@Override
	public String getUsername() {
		return null;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@JsonIgnore
	@Override
	public boolean isEnabled() {
		return false;
	}

}