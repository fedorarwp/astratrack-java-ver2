package com.javainuse.model.kategori;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.userKategori.UserKategori;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "kategori")
public class Kategori {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String judulKategori;

    @Column
    private String iconKategori;

    public Kategori() {};

    public Kategori(int id, String judulKategori, String iconKategori) {
        this.id = id;
        this.judulKategori = judulKategori;
        this.iconKategori = iconKategori;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudulKategori() {
        return judulKategori;
    }

    public void setJudulKategori(String judulKategori) {
        this.judulKategori = judulKategori;
    }

    public String getIconKategori() {
        return iconKategori;
    }

    public void setIconKategori(String iconKategori) {
        this.iconKategori = iconKategori;
    }

    @OneToMany(mappedBy = "kategori", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("kategori")
    List<UserKategori> userKategoriList = new ArrayList<>();

    public void addKategori(List<UserKategori> userKategoriList) {
        this.userKategoriList = userKategoriList;
        userKategoriList.forEach(userKategori -> userKategori.setKategori(this));
    }

    @JsonIgnore
    public List<UserKategori> getUserKategoriList() {
        return userKategoriList;
    }

    public void setUserKategoriList(List<UserKategori> userKategoriList) {
        this.userKategoriList = userKategoriList;
    }
}
